﻿using UnityEngine;

public class NewPlayerMovement : MonoBehaviour
{
    float maxSpeed = 10f;
    Rigidbody2D rigidbody2d;
    bool facingRight = true;
    bool grounded;
    float inputMove = 0f;

    //Jump
    public Transform groundCheck;
    public LayerMask whatIsGround;
    bool isJumpPressed;
    public bool isDoubleJump;
    [Range(0f, 10f)] public float JumpForceHeight = 10f;
    bool isFalling;

    //Dash
    public DashState dashState;
    public float dashTimer;
    public float MaxDashSeconds = 0.5f;
    public RigidbodyConstraints2D savedRigidBodyConstraints;
    [Range(0, 60)] public float DashingSpeed = 4f;
    public bool dashedAlreadyInTheAir = false;

    //WallJump https://www.youtube.com/watch?v=koDg2Mw1kEc
    bool isTouchingLeft;
    bool isTouchingRight;
    bool wallJumping;


    // Start is called before the first frame update
    void Awake()
    {
        rigidbody2d = GetComponent<Rigidbody2D>();
        rigidbody2d.freezeRotation = true;
    }

    // Update is called once per frame
    // Jump and Dash are in update : https://medium.com/ironequal/unity-character-controller-vs-rigidbody-a1e243591483 and https://forum.unity.com/threads/solved-inconsistent-jumping-height.515369/
    private void Update()
    {
        grounded = Physics2D.OverlapCircle(groundCheck.position, 0.2f, whatIsGround);
        inputMove = IsInputFrozen() ? 0f : tresholdMove(Input.GetAxis("Horizontal"));
        isJumpPressed = Input.GetKeyDown(KeyCode.Space) || Input.GetKeyDown(KeyCode.JoystickButton0);
        if (grounded)
        {
            isDoubleJump = false;
            isFalling = false;
            dashedAlreadyInTheAir = false;
        }

        isTouchingLeft =Physics2D.OverlapBox(new Vector2(gameObject.transform.position.x - 0.5f, gameObject.transform.position.y), new Vector2(0.3f, 0.2f), 0f, whatIsGround);
        isTouchingRight = Physics2D.OverlapBox(new Vector2(gameObject.transform.position.x + 0.5f, gameObject.transform.position.y), new Vector2(0.3f, 0.2f), 0f, whatIsGround);
        //Flip movement (not wall jump)
        if ((inputMove > 0 && !facingRight) || (inputMove < 0 && facingRight) && !IsInputFrozen())
        {
            Flip();
        }
  
        bool isPressingRight = Input.GetKey(KeyCode.D); // Input.GetAxis aka inputMove can be still non null as the player is still moving
        bool isPressingLeft = Input.GetKey(KeyCode.A);
        if (isJumpPressed && (isTouchingRight && isPressingRight || isTouchingLeft && isPressingLeft) && !grounded)
        {
            Debug.Log($"facingRight {facingRight}");
            wallJumping = true;
            rigidbody2d.velocity = new Vector2(0f, 0f); //Set no velocity to apply the walljump force only
            rigidbody2d.AddForce(new Vector2(maxSpeed * (facingRight ? -1 : 1), JumpForceHeight * 4),
                ForceMode2D.Impulse);
            Flip();
            Invoke("SetJumpingToFalse", 0.4f);
            Debug.Log("Walljumping");
        }

        HandleDashMove();
        HandleJump();
    }

    //private void OnDrawGizmosSelected()
    //{
    //    Gizmos.color = Color.green;
    //    Gizmos.DrawCube(new Vector2(gameObject.transform.position.x - 0.5f, gameObject.transform.position.y), new Vector2(0.3f, 0.2f));
    //    Gizmos.color = Color.blue;
    //    Gizmos.DrawCube(new Vector2(gameObject.transform.position.x + 0.5f, gameObject.transform.position.y), new Vector2(0.3f, 0.2f));
    //}


    private void HandleDashMove()
    {
        switch (dashState)
        {
            case DashState.Ready:
                if (IsInputFrozen())
                {
                    break;
                }

                var isDashKeyDown = Input.GetKeyDown(KeyCode.K);
                if (isDashKeyDown && !dashedAlreadyInTheAir)
                {
                    savedRigidBodyConstraints = rigidbody2d.constraints;
                    rigidbody2d.constraints = savedRigidBodyConstraints | RigidbodyConstraints2D.FreezePositionY;
                    rigidbody2d.velocity = new Vector2(facingRight ? DashingSpeed : -DashingSpeed, 0);
                    dashState = DashState.Dashing;
                }
                else
                {
                    rigidbody2d.velocity = new Vector2(inputMove * maxSpeed, rigidbody2d.velocity.y);
                }

                break;
            case DashState.Dashing:
                dashTimer += Time.deltaTime;
                if (dashTimer >= MaxDashSeconds) //Dash is finished
                {
                    dashTimer = MaxDashSeconds;
                    rigidbody2d.velocity = new Vector2(0, 0);

                    StopDashIfActive();
                    break;
                }

                break;
            case DashState.Cooldown:
                dashTimer -= Time.deltaTime * 2;
                if (dashTimer <= 0)
                {
                    dashTimer = 0;
                    dashState = DashState.Ready;
                }

                rigidbody2d.velocity = new Vector2(inputMove * maxSpeed, rigidbody2d.velocity.y);
                break;
        }
    }

    private void HandleJump()
    {
        if (isJumpPressed && !IsInputFrozen()) // The jump button must be pressed and no walljump in parallel
        {
            //Simple jump
            if (grounded)
            {
                //We jump => Stop Dashing if it is the case
                StopDashIfActive();
                //Debug.Log("first jump");
                rigidbody2d.velocity = new Vector2(rigidbody2d.velocity.x, 0f);
                rigidbody2d.AddForce(Vector2.up * JumpForceHeight * 4, ForceMode2D.Impulse);
            }
            //Double Jump
            else if (!isDoubleJump)
            {
                //We jump => Stop Dashing if it is the case
                StopDashIfActive();
                rigidbody2d.velocity = new Vector2(rigidbody2d.velocity.x, JumpForceHeight);
                isDoubleJump = true;
                Debug.Log("Double jump");
            }
        }
        // Walled jump
        else
        {
            bool isJumpStillPressed = Input.GetKey(KeyCode.Space) || Input.GetKey(KeyCode.JoystickButton0);
            if (!grounded && !isJumpStillPressed && !isFalling && !isDoubleJump)
            {
                rigidbody2d.velocity = new Vector2(rigidbody2d.velocity.x, 0);
                isFalling = true;
            }
        }

        return;
    }

    private void SetJumpingToFalse()
    {
        wallJumping = false;
    }

    /// <summary>
    /// Should we take into consideration input?
    /// </summary>
    /// <returns></returns>
    private bool IsInputFrozen()
    {
        return wallJumping;
    }

    private void StopDashIfActive()
    {
        if (dashState == DashState.Dashing)
        {
            rigidbody2d.constraints = savedRigidBodyConstraints;
            dashState = DashState.Cooldown;
            dashedAlreadyInTheAir = !grounded;
        }
    }

    /// <summary>
    /// Flip this instance.
    /// </summary>
    void Flip()
    {
        facingRight = !facingRight;
        Vector3 theScale = transform.localScale;
        theScale.x *= -1;
        transform.localScale = theScale;
    }

    /// <summary>
    /// Tresholds the move.
    /// </summary>
    /// <returns>The move.</returns>
    /// <param name="move">Move.</param>
    private float tresholdMove(float move)
    {
        if (move <= -0.5)
        {
            move = -0.5f;
        }
        else if (move > -0.5f && move <= -0.1f)
        {
            move = -0.2f;
        }
        else if (move > -0.1f && move <= 0.1f)
        {
            move = 0f;
        }
        else if (move > 0.1f && move <= 0.5f)
        {
            move = 0.2f;
        }
        else
        {
            move = 0.5f;
        }

        return move;
    }

    public enum DashState
    {
        Ready,
        Dashing,
        Cooldown
    }
}