using System.Linq;
using Helper;
using UnityEngine;

public class PlayerCombat : MonoBehaviour
{
    public Transform attackPoint;
    public float attackRange = 0.5f;
    public LayerMask EnnemyLayer;

    private void Awake()
    {
    }

    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.F))
        {
            Attack();
        }
    }

    void Attack()
    {
        //Play an attack animation
        //Detect enemies in range of attack
        var ennemyBoxes = GenerateAttackingBox();
        EnnemyScript ennemyScript;
        Debug.Log(ennemyBoxes.Length);

        foreach (var collider2DListByEnnemy in ennemyBoxes.GroupBy(j => j.gameObject))
        {
            var collider2d = collider2DListByEnnemy.First();
            ennemyScript = collider2d.gameObject.GetComponent<EnnemyScript>();
            ennemyScript.TakeHit(10);
            Debug.Log(ennemyScript.HP);
        }
        //Damage them
    }

    Collider2D[] GenerateAttackingBox()
    {
        Collider2D[]
            ennemiesBox = Physics2D.OverlapCircleAll(attackPoint.Get2dPosition(), attackRange, EnnemyLayer);
        return ennemiesBox;
    }


    private void OnDrawGizmosSelected()
    {
        if (attackPoint == null)
        {
            return;
        }

        Gizmos.color = Color.blue;
        Gizmos.DrawSphere(attackPoint.Get2dPosition(), attackRange);
    }
}