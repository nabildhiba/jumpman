﻿namespace Assets.Ennemies
{
    public interface IEnnemyScript
    {
        void TakeHit(int damage);
        void Attack();
    }
}
