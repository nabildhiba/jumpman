using Assets.Ennemies;
using UnityEngine;

public class EnnemyScript : MonoBehaviour, IEnnemyScript
{

    public int HP
    {
        get
        {
            return hp;
        }
    }
    private int hp;
    public int mp;

    public void Attack()
    {
        return;
    }

    public void TakeHit(int damage)
    {
        hp -= damage;
        if (hp <= 0)
        {
            DestroyImmediate(gameObject);
        }
    }

    private void Awake()
    {
        hp = 10;
    }



    // Update is called once per frame
    void Update()
    {

    }
}
