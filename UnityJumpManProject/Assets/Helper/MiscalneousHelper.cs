﻿using UnityEngine;
namespace Helper
{
    public static class MiscalneousHelper
    {
        public static Vector2 Get2dPosition(this Transform transform)
        {
            return new Vector2(transform.position.x, transform.position.y);
        }
    }
}
